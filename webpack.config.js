const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
// const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./src/main.js",
  mode: "development",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist"),
  },
  optimization: {
    minimizer: true,
    minimizer: [new TerserPlugin()],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "My App",
      filename: "dist/index.html",
      template: "dist/index.html",
    }),
    // new CopyPlugin({
    //   patterns: [
    //     {
    //       from: "src/styles/assets/images",
    //       to: "./",
    //     },
    //   ],
    // }),
  ],
  devServer: {
    static: { directory: path.join(__dirname, "dist") },
    port: 9000,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.css$/,
        use: [{ loader: "style-loader" }, { loader: "css-loader" }],
      },
      // {
      //   test: /\.(png)$/,
      //   use: [{ loader: "url-loader" }],
      // },
    ],
  },
};
